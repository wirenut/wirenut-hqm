:: This will create a zip file to import in twitch/multimc
:: based on the current installation
xcopy /e /v .\config .\tmp\overrides\config\
xcopy /e /v .\overrides\mods .\tmp\overrides\mods\
copy .\manifest.json .\tmp\manifest.json
cd /D .\tmp\
..\bin\7za.exe a wirenut-hqm-pack.zip .\manifest.json overrides\*
move wirenut-hqm-pack.zip ..\
cd /D ..\
rmdir /s /q .\tmp\
pause;